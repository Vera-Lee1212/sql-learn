package com.shtel.jdbc;

import com.shtel.jdbc.entity.Student;
import com.shtel.jdbc.service.StudentService;
import com.shtel.jdbc.service.StudentServiceImpl;

import java.util.List;

public class APP {
    public static void main(String[] args) {
        StudentService studentService = new StudentServiceImpl();
        List<Student> all = studentService.findAll();

        for (Student student : all) {
            System.out.println(student);
        }
        /*  studentService.insertBatch();*/

    }
}
