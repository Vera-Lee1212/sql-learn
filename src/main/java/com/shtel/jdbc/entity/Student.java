package com.shtel.jdbc.entity;

public class Student {
    //建表语句 CREATE TABLE `student` (`sno` bigint(20) NOT NULL,`name` varchar(255) NOT NULL,`age` int(11) NOT NULL,PRIMARY KEY (`sno`)) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
    //查询信息 SELECT name,age FROM student WHERE sno = 110 ;
    //修改信息 UPDATE student SET age = 16 WHERE sno = 110 ;
    private Long sno;
    private String name;
    private Integer age;

    public Long getSno(){
        return sno;
    }

    public void setSno(Long sno){
        this.sno = sno;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Integer getAge(){
        return age;
    }

    public void setAge(Integer age){
        this.age = age;
    }



    @Override
    public String toString() {
        return "Student{" +
                "sno=" + sno +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
