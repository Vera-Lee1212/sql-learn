package com.shtel.jdbc.service;

import com.shtel.jdbc.entity.Student;

import java.util.List;

public interface StudentService {

    List<Student> findAll();

    void insertStudent();

    void deleteBySno();

    void selectBySno();

    void updateNameBySno();

}
