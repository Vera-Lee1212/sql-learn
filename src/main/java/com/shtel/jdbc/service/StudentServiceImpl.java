package com.shtel.jdbc.service;

import com.shtel.jdbc.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class StudentServiceImpl implements StudentService {
    private String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
    private String username = "root";
    private String password = "lijie1212";

    private static String sql = "select * from student";

    private static String sql1 = "insert into student(sno,name,age) values (?, ?, ?)";

    private static String sql2 = "select name,age from student where sno = ?";

    private static String sql3 = "delete from student where sno = ?";

    private static String sql4 = "update student set name = ?  where sno = ?";


    static {
        try {
            // 1 加载驱动程序
            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Student> findAll() {
        List<Student> StudentList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rst = null;

        try {
            // 2 获得数据库链接
            conn = DriverManager.getConnection(url, username, password);
            // 3
            pstmt = conn.prepareStatement(sql);
            // 4
            rst = pstmt.executeQuery();
            while (rst.next()) {
                Student student = new Student();
                student.setSno(rst.getLong("sno"));
                student.setName(rst.getString("name"));
                student.setAge(rst.getInt("age"));

                StudentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, pstmt, rst);
        }

        return StudentList;
    }

    @Override
    public void insertStudent() {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            // 2 获得数据库链接
            conn = DriverManager.getConnection(url, username, password);
            // 3
            pstmt = conn.prepareStatement(sql1);
            // 4
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 1000; j++) {
                    Long k = Long.valueOf(0);
                    pstmt.setLong(1, ++k);
                    pstmt.setString(2, "aa" + "_" + j);
                    pstmt.setInt(3, 10 + i);
                    pstmt.addBatch();
                }
            }


            pstmt.executeBatch();
            //pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    pstmt = null;
                    e.printStackTrace();
                }
            }


            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    conn = null;
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void selectBySno() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rst = null;

        try {
            // 2 获得数据库链接
            conn = DriverManager.getConnection(url, username, password);
            // 3
            pstmt = conn.prepareStatement(sql2);
            // 4
            pstmt.setLong(1, 110);
            rst = pstmt.executeQuery();
            while (rst.next()) {
                Student student = new Student();
                student.setName(rst.getString("name"));
                student.setAge(rst.getInt("age"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(conn, pstmt, rst);
        }
    }


    @Override
    public void deleteBySno() {
        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            // 2 获得数据库链接
            conn = DriverManager.getConnection(url, username, password);
            // 3
            pstmt = conn.prepareStatement(sql3);
            // 4
            pstmt.setLong(1, 110);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    pstmt = null;
                    e.printStackTrace();
                }
            }


            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    conn = null;
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void updateNameBySno() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rst = null;

        try {
            // 2 获得数据库链接
            conn = DriverManager.getConnection(url, username, password);
            // 3
            pstmt = conn.prepareStatement(sql4);
            // 4
            pstmt.setString(1, "name");
            pstmt.setLong(2, 110);
            rst = pstmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    pstmt = null;
                    e.printStackTrace();
                }
            }


            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    conn = null;
                    e.printStackTrace();
                }
            }
        }

    }


        private void close (Connection conn, PreparedStatement pstmt, ResultSet rst){
            if (rst != null) {
                try {
                    rst.close();
                } catch (SQLException e) {
                    rst = null;
                    e.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    pstmt = null;
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    conn = null;
                    e.printStackTrace();
                }
            }
        }

}


